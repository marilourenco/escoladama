import java.util.ArrayList;
        import java.util.List;

public class DataBaseAluno {
    private List<Aluno> alunos = new ArrayList<>();

    void salvarAluno(Aluno aluno) {
        alunos.add(aluno);
    }

    void listarAlunos() {
        for (int i = 0; i < alunos.size(); i++) {
            Aluno aluno = alunos.get(i);
            System.out.println("Nome: " + aluno.nome + " " + "Idade: " + aluno.idade + " " + "Série: " + aluno.serie +
                    " " + "Turno: " + aluno.turno);
        }
    }

//    public void removerAluno(int index) {
//        for (int i = 0; i < alunos.size(); i++) {
//            if (alunos.get(i).getNome().equals(index)) {
//                alunos.remove(i);
//            }
//        }
//    }
}
