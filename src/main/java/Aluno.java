import java.util.ArrayList;
        import java.util.List;

public class Aluno {
    String nome;
    int idade;
    int serie;
    String turno;

    public static Aluno criarAluno(String nome, int idade, int serie, String turno) {
        Aluno aluno = new Aluno();
        aluno.nome = nome;
        aluno.idade = idade;
        aluno.serie = serie;
        aluno.turno = turno;
        return aluno;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public int getSerie() {
        return serie;
    }

    public void setSerie(int serie) {
        this.serie = serie;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }
}
