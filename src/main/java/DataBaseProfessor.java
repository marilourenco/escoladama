import java.util.ArrayList;
import java.util.List;

public class DataBaseProfessor {
    List<Professor> professores = new ArrayList<>();


    public void salvarProfessor(Professor professor) {
        professores.add(professor);
    }

    public void listarProfessor() {
        for (int i = 0; i < professores.size(); i++) {
            Professor professores = this.professores.get(i);
            System.out.println("Nome: " + professores.nome + " " + "Materia: " + professores.materia +
                    " " + "Turno: " + professores.turno);
        }
    }
}
