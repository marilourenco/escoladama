public class Professor {

    String nome;
    String materia;
    String turno;

    public static Professor criarProfessor(String nome, String materia, String turno) {
        Professor professor = new Professor();
        professor.nome = nome;
        professor.materia = materia;
        professor.turno = turno;
        return professor;
    }
}
