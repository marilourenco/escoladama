import java.awt.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DataBaseAluno dataBaseAluno = new DataBaseAluno();
        DataBaseProfessor dataBaseProfessor = new DataBaseProfessor();

        while (true) {
            System.out.println("Olá, seja bem vinda a Escola da Má!");
            System.out.println("O que você gostaria de fazer?");
            System.out.println("Digite 1 para criar um novo aluno.");
            System.out.println("Digite 2 para listar alunos.");
            System.out.println("Digite 3 para criar um novo professor.");
            System.out.println("Digite 4 para listar professores.");
            System.out.println("Digite 5 para deletar um aluno.");
            System.out.println("Digite 6 sair.");

            int opcao = scanner.nextInt();

            if (opcao == 1) {
                System.out.println("Digite o nome:");
                String nome = scanner.next();
                System.out.println("Digite a idade:");
                int idade = scanner.nextInt();
                System.out.println("Digite a série:");
                int serie = scanner.nextInt();
                System.out.println("Digite o turno:");
                String turno = scanner.next();
                Aluno alunoCriado = Aluno.criarAluno(nome, idade, serie, turno);
                dataBaseAluno.salvarAluno(alunoCriado);

            } else if (opcao == 2) {
                dataBaseAluno.listarAlunos();

            } else if (opcao == 3) {
                System.out.println("Digite o nome:");
                String nome = scanner.next();
                System.out.println("Digite a materia:");
                String materia = scanner.next();
                System.out.println("Digite a turno:");
                String turno = scanner.next();
                Professor professorCriado = Professor.criarProfessor(nome, materia, turno);
                dataBaseProfessor.salvarProfessor(professorCriado);

            } else if (opcao == 4) {
                dataBaseProfessor.listarProfessor();

//            } else if (opcao == 5) {
//                System.out.println("Digite o número correspondente ao aluno");
//                int numeroAluno = scanner.nextInt();
//                dataBaseAluno.removerAluno(numeroAluno);

            }else {
                break;
            }
        }
    }
}

